# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

"""
Details

For example given this  URL http://www.seloger.com/list.htm?ci=920026&idtt=1&idtypebien=1,2&LISTING-LISTpg=4 

The scraper should return for each flat the following data:
-flat name
-city
-price
-size
-gps location
"""

class SelogerItem(scrapy.Item):
	title = scrapy.Field()
	city = scrapy.Field()
	price = scrapy.Field()
	size = scrapy.Field()
	lat = scrapy.Field()
	lon = scrapy.Field()
