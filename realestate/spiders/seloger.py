# -*- coding: utf-8 -*-

import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Join, TakeFirst

from realestate.items import SelogerItem
from ..loaders import SelogerItemLoader


class SelogerSpider(CrawlSpider):
    """
    French realestate portal http://www.seloger.com
    """
    name = 'seloger'
    allowed_domains = ['seloger.com']
    start_urls = [
        'http://www.seloger.com/list.htm?div=2238&idtt=1&idtypebien=1,10,2&bd=DetailToList_SL'
    ]

    rules = (
        # next page rule
        Rule(
            LinkExtractor(restrict_css='a.pagination_next.active'),
        ),
        # item rule
        Rule(
            LinkExtractor(restrict_xpaths="//*[@class='listing_infos']"), 
            callback='parse_item', 
        ),
    )

    def parse_item(self, response):
 
        loader = SelogerItemLoader(response=response)

        ## TODO: scrap title when clien will be sure about its representation
        # loader.add_xpath('title', 
        #     "//title/text()")

        loader.add_xpath('city',
            "//h2[contains(@class, 'detail-subtitle')]/span/text()", 
            re=r'\w+\s(.+)')
        
        loader.add_xpath('price',
            "//*[@id='price']/text()", 
            re=r'\d+')
        
        loader.add_xpath('size',
            "//*[contains(@title, 'Surface de')]/text()",
            re=r'\d+')
        
        # actual coords (may be blank)
        loader.add_xpath('lat', 
            "//*[contains(@id, 'map')]/@data-coordonnees-latitude")
        loader.add_xpath('lon', 
            "//*[contains(@id, 'map')]/@data-coordonnees-longitude")

        # Fields for coords approximation
        loader.add_xpath('ne_lat', 
            "//*[contains(@id, 'map')]/@data-boudingbox-northeast-latitude")
        loader.add_xpath('ne_lon', 
            "//*[contains(@id, 'map')]/@data-boudingbox-northeast-longitude")

        loader.add_xpath('sw_lat', 
            "//*[contains(@id, 'map')]/@data-boudingbox-southwest-latitude")
        loader.add_xpath('sw_lon', 
            "//*[contains(@id, 'map')]/@data-boudingbox-southwest-longitude")

        return loader.load_item()
