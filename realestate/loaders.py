import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose, Compose, Join, TakeFirst
from .items import SelogerItem

def truncate_processor(x):
    """
    Truncates float to 5 deimal places
    """
    return round(x, 5)

class SelogerItemLoader(ItemLoader):
    """
    Item loader with lat/lon check and output processors
    """
    default_item_class = SelogerItem

    city_out = TakeFirst()

    # convert latitude/longitude to floats on output
    lat_out = MapCompose(float, truncate_processor)
    lat_out = Compose(lat_out, TakeFirst())
    
    lon_out = MapCompose(float, truncate_processor)
    lon_out = Compose(lon_out, TakeFirst())

    # cleanup price
    price_out = Compose(Join(separator=''), float)

    # cleanup floor size
    size_in = MapCompose(int)
    size_out = TakeFirst()

    # title
    title_out = Compose(Join())

    def __init__(self, *args, **kwargs):
        super(SelogerItemLoader, self).__init__(*args, **kwargs)

        # attach fields for coords approximation
        self.item.fields['sw_lat'] = scrapy.Field()
        self.item.fields['sw_lon'] = scrapy.Field()
        
        self.item.fields['ne_lat'] = scrapy.Field()
        self.item.fields['ne_lon'] = scrapy.Field()

    def load_item(self):
        """
        Makes approximation of lat/lon if actual value is not found
        then loads an item
        """
        # print 'BEFORE:', self._values
        
        # avoid IndexError
        lat = self._values['lat']
        lat = lat[0] if len(lat) else 0
        lon = self._values['lon']
        lon = lon[0] if len(lon) else 0

        if not lat:
            approx_lat = self._approximate_geo('sw_lat', 'ne_lat')
            self.replace_value('lat', approx_lat)
            # print 'LAT:', self.get_output_value('lat')

        if not lon:
            approx_lon = self._approximate_geo('sw_lon', 'ne_lon')            
            self.replace_value('lon', approx_lon)           
            # print 'LON:', self.get_output_value('lon')

        self._cleanup()     

        # print 'AFTER:', self._values

        return super(SelogerItemLoader, self).load_item()

    def _approximate_geo(self, coord_field_1, coord_field_2):
        """
        Returns middle between 2 coordinates
        """
        # get lists with values
        coord_1 = self.get_output_value(coord_field_1)
        coord_2 = self.get_output_value(coord_field_2)

        # print coord_field_1,  coord_1, coord_field_2,  coord_2

        return sum(float(x) for x in coord_1+coord_2)/2.0

    def _cleanup(self):
        """
        Removes additional data from Loader to prevent Item clogging
        Restore original Item's fields
        """
        for field in ['sw_lat', 'ne_lat', 'sw_lon', 'ne_lon']:
            try:
                del self._values[field]
                del self.item.fields[field]
            except KeyError, e:
                pass

        # self.item.fields = self._original_fields
